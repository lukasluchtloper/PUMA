\documentclass[kulak,a4paper,12pt]{kulakarticle}
\author{Brecht Vandenborre, Lukas Vinoelst}
\date{\today}
\title{Scaling laws for the PUMA-magnet}

\usepackage{flafter}
\usepackage{xcolor, graphicx}
\usepackage[english]{babel}
\usepackage{subfig}
\usepackage{csquotes}
\usepackage{epstopdf}
\usepackage{amsmath}
\usepackage{float}
\usepackage{physics}

\begin{document}
\maketitle
\section*{introduction}

\section{Working with and implementing of an FEM-solver}
Before we can begin on determining the scaling laws of the PUMA magnet, it is important that we get a bit of background knowledge on how to calculate and simulate magnetic fields. We use an exercise \cite{oefening} and the accompanying theoretical lesson \cite{theory}, both of which are given at CERN, to achieve this goal and to learn how to work with an FEM (Finite Element Method) solver. First a simulation of the magnetic field in the SIS100 accelerator magnet is made in the free simulation software Femm, after which the mathematical method of FEM is deduced and then used to implement an own FEM solver in Matlab.

\subsection{FEMM}
First of all we model the SIS100 magnet in Femm, a free FEM solver. The modelling happens step by step. First the co\"ordinates of individual points are put in, then they are connected with straight or curved lines. For the next step a material is assigned to the different area's of the magnet. These are: iron, air and copper. The coils through which the current of 6045.75 Amp flows are hollow. This means that they are made of an air gap encircled by a copper mantle, as can be seen on the close-up image in figure\ref{fig: sis100}. Eventually we get a model as shown in figure\ref{fig: sis100}.\\

\begin{figure}[H]
	\centering
	\subfloat[The complete SIS100 magnet model]{\includegraphics[scale=0.5]{sis100}}
	\qquad
	\subfloat[A close-up of the coils]{\includegraphics[scale=0.4]{close_up}}
	\caption{The SIS100 magnet in Femm}
	\label{fig: sis100}
\end{figure}

Now we can begin to apply boundary conditions on the different edges. We have two types, the Dirichlet and Neumann boundary conditions (BC's). The first of these corresponds to magnetic flux-lines not leaving the iron yoke. While the Neumann BC's correspond to magnetic flux-lines standing perpendicular on the interface, which is important since we make use of the symmetric properties of the magnet and can therefore only model one quarter of it to reduce calculating time. After applying these BC's we can now go ahead and apply a mesh on this model and run the solver. As a result we get a more or less homogeneous magnetic field in the aperture area of about 1.961 T. The air gap above the coils helps to make the field more homogeneous. The results of the simulation can be seen in figure\ref{fig: sis100_result}. It is clear that the field density is highest in the aperture area, which it should be since that is the area where the accelerated particles are moving and should be constrained.
  
\begin{figure}[H]
	\centering
	\subfloat[The result with the flux-density shown]{\includegraphics[scale=0.4]{flux_density}}
	\qquad
	\subfloat[The result with the field-density shown]{\includegraphics[scale=0.4]{field_density}}
	\caption{The results of the Femm simulation}
	\label{fig: sis100_result}
\end{figure}

\subsection{Mathematical deduction}
Of course we have to deduce and calculate the necessary mathematics if we want to implement our own FEM solver. We start with the Maxwell equations, and work our way to the formulas needed for the FEM solver. We do this by applying the weighted residual method.

\subsubsection{From continuous to discrete}
We start with the Maxwell equations:

\begin{gather}\label{eq: maxwell}
	\div \va{D} = \rho \\
	\curl{\va{H}} = \va{J} + \pdv{\va{D}}{t} \\
	\div \va{B} = 0 \\
	\curl{\va{E}} = -\pdv{\va{B}}{t}
\end{gather}

First we take the second equation from \ref{eq: maxwell} and neglect the electrical energy with respect to the magnetic energy. That way we get the following:
\begin{equation} \label{eq: Emag}
	\curl{\va{H}} = \va{J}
\end{equation}
Then we get to the third equation from \ref{eq: maxwell} and integrate over the volume. We use Gauss and get that
\begin{equation}\label{eq: mag}
	\va{B} = \curl{\va{A}}
\end{equation}
Last we take the fourth equation from \ref{eq: maxwell} and apply the same procedure as with the above equation. We then get
\begin{equation}\label{eq: electric}
	\va E = - \pdv{\va{A}}{t} - \grad V
\end{equation}

To get the magnetostatic formula we require let us use the material laws $\va{B} = \mu \va{H}$ and $\va{J} = \sigma \va{E}$. Via combination with \ref{eq: Emag}, \ref{eq: mag} and \ref{eq: electric} we get the magnetostatic equation
\begin{equation}\label{eq: magnetostatic weak}
	\curl (\nu \curl \va{A}) + \sigma \pdv{\va{A}}{t} + \sigma \grad \va V = 0
\end{equation}
Or in the strong version:
\begin{equation}\label{eq: magnetostatic strong}
\curl (\nu \curl \va{A}) + \sigma \pdv{\va{A}}{t} = \va J\textsubscript{s}
\end{equation}\\

The equation we now have is a continuous one, but for the FE solver this needs to be discrete. We achieve this by applying the weighted residual method. We multiply \ref{eq: magnetostatic strong} with test functions $\va w\textsubscript{i}(\va r)$ and integrate over the computational domain V. After applying some vector calculus and the Gauss theorem we get
\begin{equation}
	\int_{\var{V}} (\nu \curl \va{A}) \cross \va w\textsubscript{i}(\va r) d\va S + \int_{V} (\nu \curl \va{A})(\curl \va w\textsubscript{i}(\va r)) d\va V + \int_{V}\sigma \pdv{\va{A}}{t}\va w\textsubscript{i}(\va r) d\va V = \int_{V}\va J\textsubscript{s}\va w\textsubscript{i}(\va r) d\va V
\end{equation}
When we apply the Dirichlet and Neumann boundary conditions we can remove the first term on the left of this equation. The resulting formulation is the weak form of the magnetoquasistatic equation. In this formulation we now insert the discretisation of the magnetic vector potential $\va{A} = \sum_{j} \va{u}\textsubscript{j}\va w\textsubscript{j}(\va r)$. We can now also determine the co\"efficients to simplify this equation.
\begin{gather}
	K\textsubscript{$\nu$, ij} = \int_{V} (\nu \curl \va w\textsubscript{j}(\va r))(\curl \va w\textsubscript{i}(\va r)) dV \\
	M\textsubscript{$\sigma$, ij} = \int_{V}\sigma \pdv{\va w\textsubscript{j}(\va r)}{t}\va w\textsubscript{i}(\va r) dV \\
	f\textsubscript{s, i} = \int_{V}\va J\textsubscript{s}\va w\textsubscript{i}(\va r) dV
\end{gather}
When we implement these co\"efficients in the equation, we get the following system of equations.
\begin{equation}
	K\textsubscript{$\nu$, ij}\va{u}\textsubscript{j} + M\textsubscript{$\sigma$, ij}\pdv{\va{u}\textsubscript{j}}{t} = f\textsubscript{s, i}
\end{equation}



\bibliography{articlesPUMA}
\bibliographystyle{ieeetr}

\end{document}